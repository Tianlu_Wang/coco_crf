import torch.nn as nn
import itertools
import numpy as np
import torch
from torch.autograd import Variable
import pdb
import torch.autograd as autograd
import torch.optim as optim
import pickle
import os

id2object = {0: 'toilet', 1: 'teddy_bear', 2: 'sports_ball', 3: 'bicycle', 4: 'kite', 5: 'skis', 6: 'tennis_racket', 7: 'donut', 8: 'snowboard', 9: 'sandwich', 10: 'motorcycle', 11: 'oven', 12: 'keyboard', 13: 'scissors', 14: 'chair', 15: 'couch', 16: 'mouse', 17: 'clock', 18: 'boat', 19: 'apple', 20: 'sheep', 21: 'horse', 22: 'giraffe', 23: 'person', 24: 'tv', 25: 'stop_sign', 26: 'toaster', 27: 'bowl', 28: 'microwave', 29: 'bench', 30: 'fire_hydrant', 31: 'book', 32: 'elephant', 33: 'orange', 34: 'tie', 35: 'banana', 36: 'knife', 37: 'pizza', 38: 'fork', 39: 'hair_drier', 40: 'frisbee', 41: 'umbrella', 42: 'bottle', 43: 'bus', 44: 'zebra', 45: 'bear', 46: 'vase', 47: 'toothbrush', 48: 'spoon', 49: 'train', 50: 'airplane', 51: 'potted_plant', 52: 'handbag', 53: 'cell_phone', 54: 'traffic_light', 55: 'bird', 56: 'broccoli', 57: 'refrigerator', 58: 'laptop', 59: 'remote', 60: 'surfboard', 61: 'cow', 62: 'dining_table', 63: 'hot_dog', 64: 'car', 65: 'cup', 66: 'skateboard', 67: 'dog', 68: 'bed', 69: 'cat', 70: 'baseball_glove', 71: 'carrot', 72: 'truck', 73: 'parking_meter', 74: 'suitcase', 75: 'cake', 76: 'wine_glass', 77: 'baseball_bat', 78: 'backpack', 79: 'sink'}

def inference(output):
    results = list() # log object name
    results_num = list() # log rebuslt object index 
    for i in range(len(output)):
        
        output_one = output[i]
        man_score = output_one[0]
        woman_score = output_one[1]
        man_objects = output_one[2:162]
        woman_objects = output_one[162:]
        man_index = list()
        woman_index = list()

        for j in range(80):
            if man_objects[j*2] > man_objects[j*2+1]:
                man_index.append(j)
                man_score += man_objects[j*2]
            else:
                man_score += man_objects[j*2+1]

        for j in range(80):
            if woman_objects[j*2] > woman_objects[j*2+1]:
                woman_index.append(j)
                woman_score += woman_objects[j*2]
            else:
                woman_score += woman_objects[j*2+1]

        result = list()
        result_num = [0]*81
        if man_score > woman_score:
            result.append("man")
            for elem in man_index:
                result.append(id2object[elem])
                result_num[elem+1] = 1
        else:
            result.append("woman")
            result_num[0] = 1 
            for elem in woman_index:
                result.append(id2object[elem])
                result_num[elem+1] =1
        results.append(result)
        results_num.append([str(elem) for elem in result_num])

    return results, results_num

def parse_ground_truth(target):
    results = list()
    for i in range(len(target)):
        target_one = target[i]
        man_score = target_one[0]
        woman_score = target_one[1]
        man_objects = target_one[2:162]
        woman_objects = target_one[162:]
        man_index = list()
        woman_index = list()

        for j in range(80):
            if man_objects[j*2] > man_objects[j*2+1]:
                man_index.append(j)
                man_score += man_objects[j*2]
            else:
                man_score += man_objects[j*2+1]

        for j in range(80):
            if woman_objects[j*2] > woman_objects[j*2+1]:
                woman_index.append(j)
                woman_score += woman_objects[j*2]
            else:
                woman_score += woman_objects[j*2+1]

        result = list()
        if man_score > woman_score:
            result.append("man")
            for elem in man_index:
                result.append(id2object[elem])
        else:
            result.append("woman")
            for elem in woman_index:
                result.append(id2object[elem])
        results.append(result)
    
    return results

def write_file(pred, target, image_paths, file_name, split, args):
    predictions, predictions_num = inference(pred)
    groundtruth = parse_ground_truth(target)
    for i in range(len(predictions)):
        img = str(image_paths[i])
        file_name.write("****************************" + "\n" + img+"\n"+"prediction:"+"\n"+ " ".join(predictions[i])+"\n" 
            +"prediction_num:"+"\n"+ " ".join(predictions_num[i])+"\n"+ "groundtruth:"+"\n" + " ".join(groundtruth[i])+"\n")