import json, os, string, random, time, pickle, gc
from PIL import Image
import numpy as np
import torch
import torch.nn as nn
import torch.utils.data as data
import torchvision.transforms as transforms

class CocoCRF(data.Dataset):
    def __init__(self,annotation_dir, image_dir, split = 'train', transform = None, single_batch = 0):
        self.split = split
        self.image_dir = image_dir
        self.transform = transform
        self.single_batch = single_batch

        print("loading %s annotations.........." % self.split)
        self.ann_data = pickle.load(open(os.path.join(annotation_dir, split+".data")))
        if single_batch:
            random.shuffle(self.ann_data)
            self.ann_data = self.ann_data[:single_batch]

    def __getitem__(self, index):
        img = self.ann_data[index]
        if self.split == 'train':
            image_path_ = os.path.join(self.image_dir,"train2014", img['img'])
        else:
            image_path_ = os.path.join(self.image_dir,"val2014", img['img'])
        img_ = Image.open(image_path_).convert('RGB')
        if self.transform is not None:
            img_ = self.transform(img_)
        return img_, torch.Tensor(self.ann_data[index]['annotation']), index

    def __len__(self):
        print("length of data:...")
        print(len(self.ann_data))
        return len(self.ann_data)


def load_data(args, trainTransform, testTransform):
    train_data = CocoCRF(args.ann_dir, args.image_dir, split = 'train', 
        transform = trainTransform, single_batch = args.single_batch)
    val_data = CocoCRF(args.ann_dir, args.image_dir, split="dev",
        transform=testTransform, single_batch=args.single_batch)
    return train_data, val_data

def load_testData(args, testTransform):
    cached_file = 'cached_' + args.dataset + '.p'
    print("loading cached data...")
    (train_data, _) = pickle.load(open(cached_file))
    test_data = CocoCRF(args.ann_dir, args.image_dir, split='test',
        transform=testTransform, single_batch=args.single_batch)

    return train_data, test_data