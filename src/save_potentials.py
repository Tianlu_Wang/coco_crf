import argparse
import torch

import torch.nn as nn
import torch.optim as optim
import torch.backends.cudnn as cudnn

import torch.nn.functional as F
from torch.autograd import Variable

import torchvision.datasets as dset
import torchvision.transforms as transforms
from torchvision.utils import save_image
from torch.utils.data import DataLoader
from sklearn.metrics import average_precision_score

import os, sys, math, random, pdb, random, string, shutil, time, pickle
import numpy as np

import data_loader
import model
import my_criterion
import inference
import pickle
from train import accuracy


scaling_parameter = 1.0
num_verb = 2
num_role = 80
num_value = 2

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--dataset', type = str, default = 'coco_gender_obejct')
    parser.add_argument('--batch_size', type = int, default = 64)
    parser.add_argument('--n_epochs', type = int, default = 30)
    parser.add_argument('--start_epoch', type = int, default = 1)
    parser.add_argument('--single_batch', type = int, default = 0) 
    parser.add_argument('--image_size', type = int, default = 256)
    parser.add_argument('--crop_size', type = int, default = 224)
    parser.add_argument('--seed', type = int, default = 1)
    parser.add_argument('--learning_rate', type = float, default = 1e-3) # need to change
    parser.add_argument('--ann_dir', default = '/if24/tw8cb/PhD/gender_bias/coco_crf/data/annotations')
    parser.add_argument('--image_dir', default = '/localtmp/data/coco')
    parser.add_argument('--log_dir', default = '/localtmp/tw8cb')
    parser.add_argument('--pretrained', default = '')
    parser.add_argument('--parse_result', default = 1)
    parser.add_argument('--resume', default = 0, type = int,
                        help = 'whether to resume from log_dir if existent')

    args = parser.parse_args()
    torch.manual_seed(args.seed)
    torch.cuda.manual_seed(args.seed)


    normMean = [0.49139968, 0.48215827, 0.44653124]
    normStd = [0.24703233, 0.24348505, 0.26158768]
    normTransform = transforms.Normalize(normMean, normStd)


    testTransform = transforms.Compose([
        transforms.Scale(args.image_size),
        transforms.CenterCrop(args.crop_size),
        transforms.ToTensor(),
        normTransform
    ])

    trainData, testData = data_loader.load_testData(args, testTransform)

    test_loader = DataLoader(testData, batch_size=args.batch_size, shuffle=False,
        pin_memory = False, num_workers = 4)

    print("prepare model...")
    net = model.CocoObjectCRF(num_verb, num_role, num_value)
    net = nn.DataParallel(net).cuda()

    if os.path.isfile(os.path.join(args.log_dir, 'model_best.pth.tar')):
        print("=> loading checkpoint '{}'".format(args.resume))
        checkpoint = torch.load(os.path.join(args.log_dir, 'model_best.pth.tar'))
        args.start_epoch = checkpoint['epoch']
        best_error = checkpoint['neg_f1_score']
        net.load_state_dict(checkpoint['state_dict'])
        print("=> loaded checkpoint (epoch {})".format(checkpoint['epoch']))
    else:
        print("=> no checkpoint found at '{}'".format(args.log_dir))
        return

    print('\nNumber of params: {}'.format(sum([p.data.nelement() for p in net.parameters()])))

    criterion = my_criterion.CRF_Loss(num_verb, num_role)
    criterion = criterion.cuda()

    cudnn.benchmark = True

    
    test(args, args.start_epoch, net, criterion, test_loader)

def test(args, epoch, net, criterion, test_loader):
    net.eval()
    nProcessed = 0
    nTrain = len(test_loader.dataset)

    results = list()
    potentials = list()
    for batch_idx, (data, target, image_ids) in enumerate(test_loader):
        # prepare inputs.
        data, target = data.cuda(), target.cuda()

        # Forward pass.
        data, target = Variable(data, volatile=True), Variable(target)
        output = net(data)
        results.append((output.data.cpu(), target.data.cpu()))
        potentials.append({"batch_idx": batch_idx, "image_ids":image_ids.numpy(), "output": output.data.cpu().numpy()})  # index is also a tensor
        # Backward pass and optimization step.
        loss = criterion(output, target)
    
        # Compute logging information.
        nProcessed += len(data)
        print('Train Epoch: {} [{}/{} ({:.0f}%)]\t'.format(epoch, nProcessed, nTrain, 100. * batch_idx / len(test_loader)))

    pickle.dump(potentials, open(os.path.join(args.log_dir, 'potentials_test'), "wb"))
    predictions = torch.cat([entry[0] for entry in results], 0)
    targets = torch.cat([entry[1] for entry in results], 0)
    _, _, _, f1_unknown_gender, _, _ = accuracy(predictions, targets)
    _, pred_train = inference.inference(predictions.tolist())
    pickle.dump(pred_train, open(os.path.join(args.log_dir, 'pred'), "wb"))

if __name__ == '__main__':
    main()