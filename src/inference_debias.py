import itertools
import numpy as np
import pdb
import pickle
import os

id2object = {0: 'toilet', 1: 'teddy_bear', 2: 'sports_ball', 3: 'bicycle', 4: 'kite', 5: 'skis', 6: 'tennis_racket', 7: 'donut', 8: 'snowboard', 9: 'sandwich', 10: 'motorcycle', 11: 'oven', 12: 'keyboard', 13: 'scissors', 14: 'chair', 15: 'couch', 16: 'mouse', 17: 'clock', 18: 'boat', 19: 'apple', 20: 'sheep', 21: 'horse', 22: 'giraffe', 23: 'person', 24: 'tv', 25: 'stop_sign', 26: 'toaster', 27: 'bowl', 28: 'microwave', 29: 'bench', 30: 'fire_hydrant', 31: 'book', 32: 'elephant', 33: 'orange', 34: 'tie', 35: 'banana', 36: 'knife', 37: 'pizza', 38: 'fork', 39: 'hair_drier', 40: 'frisbee', 41: 'umbrella', 42: 'bottle', 43: 'bus', 44: 'zebra', 45: 'bear', 46: 'vase', 47: 'toothbrush', 48: 'spoon', 49: 'train', 50: 'airplane', 51: 'potted_plant', 52: 'handbag', 53: 'cell_phone', 54: 'traffic_light', 55: 'bird', 56: 'broccoli', 57: 'refrigerator', 58: 'laptop', 59: 'remote', 60: 'surfboard', 61: 'cow', 62: 'dining_table', 63: 'hot_dog', 64: 'car', 65: 'cup', 66: 'skateboard', 67: 'dog', 68: 'bed', 69: 'cat', 70: 'baseball_glove', 71: 'carrot', 72: 'truck', 73: 'parking_meter', 74: 'suitcase', 75: 'cake', 76: 'wine_glass', 77: 'baseball_bat', 78: 'backpack', 79: 'sink'}
"""this script is to be interated with Lagrange regularization, no tensor here"""
def inference(output):
    """output should be list, size num_sample*322""" 
    results = list()
    results_num = list()
    for i in range(len(output)):
        output_one = output[i]
        man_score = output_one[0]
        woman_score = output_one[1]
        man_objects = output_one[2:162]
        woman_objects = output_one[162:]
        man_index = list()
        woman_index = list()

        for j in range(80):
            if man_objects[j*2] > man_objects[j*2+1]:
                man_index.append(j)
                man_score += man_objects[j*2]
            else:
                man_score += man_objects[j*2+1]

            if woman_objects[j*2] > woman_objects[j*2+1]:
                woman_index.append(j)
                woman_score += woman_objects[j*2]
            else:
                woman_score += woman_objects[j*2+1]

        result = list()
        result_num = [0]*81
        if man_score > woman_score:
            result.append("man")
            for elem in man_index:
                result.append(id2object[elem])
                result_num[elem+1] = 1
        else:
            result.append("woman")
            result_num[0] = 1 
            for elem in woman_index:
                result.append(id2object[elem])
                result_num[elem+1] =1
        results.append(result)
        results_num.append([str(elem) for elem in result_num])

    return results, results_num

from sklearn.metrics import f1_score
from sklearn.metrics import precision_score
from sklearn.metrics import recall_score

def accuracy(output, target):
    """output and target should be numpy array, num_sample*322"""
    output_gender = output[:, :2]
    target_gender = target[:, :2]
    output_objects = output[:, 2:].reshape(len(output), 2,80,2)
    target_objects = target[:, 2:].reshape(len(output), 2,80,2)

    #given man/woman
    pred_scores_known = np.zeros((len(output),1, 80, 2))
    for i in range(len(output)):
        if target_gender[i, 1] == 0: # man
            pred_scores_known[i, 0, :, :] = output_objects[i, 0, :, :]
        else:
            pred_scores_known[i, 0, :, :] = output_objects[i, 1, :, :]

    pred_idx_known = np.argmin(pred_scores, 3)
    pred_idx_known = pred_idx.squeeze() #batch_size*80

    target_scores = np.zeros((len(output),1, 80, 2))
    for i in range(len(output)):
        if target_gender[i, 1] == 0:
            target_scores[i, 0, :, :] = target_objects[i, 0, :, :]
        else:
            target_scores[i, 0, :, :] = target_objects[i, 1, :, :]

    target_idx = np.argmin(target_scores, 3)
    target_idx = target_idx.squeeze()

    pred_scores_unknown = np.zeros((len(output),1, 80, 2))
    for i in range(len(output)):
        if output_gender[i, 1] == 0:
            pred_scores_unknown[i, 0, :, :] = output_objects[i, 0, :, :]
        else:
            pred_scores_unknown[i, 0, :, :] = output_objects[i, 1, :, :]
    pred_idx_unknown = np.argmin(pred_scores_unknown, 3)
    pred_idx_unknown = pred_idx_unknown.squeeze() #batch_size*80

    f1_known_gender = f1_score(target_idx, pred_idx_known, average='macro')
    # f1 score for every class
    f1_class = list()
    for i in range(80):
        f1_class.append(f1_score(target_idx[:,i], pred_idx_known[:, i]))
    print(f1_class)
    print(sum(f1_class) / float(len(f1_class))) # should be same as f1_known_gender

    recall_known_gender = recall_score(target_idx, pred_idx_known, average='macro')  
    precision_known_gender = precision_score(target_idx, pred_idx_known, average='macro')

    f1_unknown_gender = f1_score(target_idx, pred_idx_unknown, average='macro')
    recall_unknown_gender = recall_score(target_idx, pred_idx_unknown, average='macro')  
    precision_unknown_gender = precision_score(target_idx, pred_idx_unknown, average='macro')

    return f1_known_gender, precision_known_gender, recall_known_gender, f1_unknown_gender, precision_unknown_gender, recall_unknown_gender

if __name__=='__main__':
    main()