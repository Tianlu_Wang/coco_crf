import argparse
import torch

import torch.nn as nn
import torch.optim as optim
import torch.backends.cudnn as cudnn

import torch.nn.functional as F
from torch.autograd import Variable

import torchvision.datasets as dset
import torchvision.transforms as transforms
from torchvision.utils import save_image
from torch.utils.data import DataLoader
from sklearn.metrics import average_precision_score

import os, sys, math, random, pdb, random, string, shutil, time, pickle
import numpy as np

import data_loader
import model
import my_criterion
import inference
import pickle


scaling_parameter = 1.0
num_verb = 2 # man, womam
num_role = 80 # objects
num_value = 2 # yes, no

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--dataset', type = str, default = 'coco_gender_obejct')
    parser.add_argument('--batch_size', type = int, default = 64)
    parser.add_argument('--n_epochs', type = int, default = 150)
    parser.add_argument('--start_epoch', type = int, default = 1)
    parser.add_argument('--single_batch', type = int, default = 0) 
    parser.add_argument('--image_size', type = int, default = 256)
    parser.add_argument('--crop_size', type = int, default = 224)
    parser.add_argument('--seed', type = int, default = 1)
    parser.add_argument('--learning_rate', type = float, default = 1e-5) # need to change
    parser.add_argument('--ann_dir', default = '/if24/tw8cb/PhD/gender_bias/coco_crf/data/annotations')
    parser.add_argument('--image_dir', default = '/localtmp/data/coco')
    parser.add_argument('--log_dir', default = '/localtmp/tw8cb')
    parser.add_argument('--pretrained', default = '')
    parser.add_argument('--parse_result', default = 1, help='')
    parser.add_argument('--resume', default = 0, type = int,
                        help = 'whether to resume from log_dir if existent')

    args = parser.parse_args()
    torch.manual_seed(args.seed)
    torch.cuda.manual_seed(args.seed)

    if os.path.exists(args.log_dir) and not args.resume:
        print("Path {} exists! and not resuming".format(args.log_dir))
        return
    if not os.path.exists(args.log_dir): os.makedirs(args.log_dir)

    normMean = [0.49139968, 0.48215827, 0.44653124]
    normStd = [0.24703233, 0.24348505, 0.26158768]
    normTransform = transforms.Normalize(normMean, normStd)

    trainTransform = transforms.Compose([
        transforms.Scale(args.image_size),
        transforms.RandomCrop(args.crop_size),
        transforms.RandomHorizontalFlip(),
        transforms.ToTensor(),
        normTransform
    ])
    testTransform = transforms.Compose([
        transforms.Scale(args.image_size),
        transforms.CenterCrop(args.crop_size),
        transforms.ToTensor(),
        normTransform
    ])

    cached_file = 'cached_' + args.dataset + '.p'
    if os.path.exists(cached_file) and not args.single_batch:
        print('loading cached data...')
        (train_data, val_data) = pickle.load(open(cached_file))
        train_data.transform = trainTransform
        val_data.transform = testTransform
    else:
        train_data, val_data = data_loader.load_data(args, trainTransform, testTransform)
        if not args.single_batch:pickle.dump((train_data, val_data), open(cached_file, 'w'))

    train_loader = DataLoader(train_data, batch_size=args.batch_size, shuffle=True,
        pin_memory = True, num_workers = 6)
    test_loader = DataLoader(val_data, batch_size=args.batch_size, shuffle=False,
        pin_memory = False, num_workers = 4)

    print("prepare model...")
    net = model.CocoObjectCRF(num_verb, num_role, num_value)
    net = nn.DataParallel(net).cuda()

    
    if args.resume:
        trainF = open(os.path.join(args.log_dir, 'train.csv'), 'a')
        train_log = open(os.path.join(args.log_dir, 'train.log'), 'a') # to log predictions human understandable
        testF = open(os.path.join(args.log_dir, 'test.csv'), 'a')
        test_log = open(os.path.join(args.log_dir, 'test.log'), 'a')
        f1score_log = open(os.path.join(args.log_dir, 'f1score.csv'), 'a')
        if os.path.isfile(os.path.join(args.log_dir, 'checkpoint.pth.tar')):
            print("=> loading checkpoint '{}'".format(args.resume))
            checkpoint = torch.load(os.path.join(args.log_dir, 'checkpoint.pth.tar'))
            args.start_epoch = checkpoint['epoch']
            best_error = checkpoint['neg_f1_score']
            net.load_state_dict(checkpoint['state_dict'])
            print("=> loaded checkpoint (epoch {})".format(checkpoint['epoch']))
        else:
            print("=> no checkpoint found at '{}'".format(args.resume))
    else:
        trainF = open(os.path.join(args.log_dir, 'train.csv'), 'w')
        train_log = open(os.path.join(args.log_dir, 'train.log'), 'w')
        testF = open(os.path.join(args.log_dir, 'test.csv'), 'w')
        test_log = open(os.path.join(args.log_dir, 'test.log'), 'w')
        f1score_log = open(os.path.join(args.log_dir, 'f1score.csv'), 'w')

    print('\nNumber of params: {}'.format(
        sum([p.data.nelement() for p in net.parameters()])))

    criterion = my_criterion.CRF_Loss(num_verb, num_role)
    criterion = criterion.cuda()

    cudnn.benchmark = True

    def get_trainable_parameters(model):
        for param in model.parameters():
            if param.requires_grad:
                yield param

    optimizer = optim.SGD(get_trainable_parameters(net), lr = args.learning_rate, 
                          momentum = 0.9, weight_decay = 1e-4)
    # optimizer = optim.Adam(get_trainable_parameters(net), lr = args.learning_rate)
    best_error = 1 # error is 1-f_score
    for epoch in range(args.start_epoch, args.n_epochs + 1):
        train(args, epoch, net, criterion, train_loader, optimizer, trainF, train_log, f1score_log)
        error = test(args, epoch, net, criterion, test_loader, optimizer, testF, test_log, f1score_log)
        is_best = error < best_error
        best_error = min(error, best_error)
        save_checkpoint(args, {
            'epoch': epoch + 1,
            'state_dict': net.state_dict(),
            'neg_f1_score': error
        }, is_best, os.path.join(args.log_dir, 'checkpoint.pth.tar'))
        os.system('python plot.py {} &'.format(args.log_dir))

    trainF.close()
    testF.close()
    train_log.close()
    test_log.close()
    f1score_log.close()

def save_checkpoint(args, state, is_best, filename):
    torch.save(state, filename)
    if is_best:
        shutil.copyfile(filename, os.path.join(args.log_dir, 'model_best.pth.tar'))
        print("-------------------copy into model best checkpoint done------------------")

def train(args, epoch, net, criterion, train_loader, optimizer, trainF, train_log, f1score_log):
    net.train()
    nProcessed = 0
    nTrain = len(train_loader.dataset)
    batch_time = AverageMeter()
    data_time = AverageMeter()
    loss_logger = AverageMeter()
    error_logger = AverageMeter()

    results = list()

    end = time.time()
    
    for batch_idx, (data, target, image_ids) in enumerate(train_loader):
        # measure data loading time
        data_time.update(time.time() - end)

        # prepare inputs.
        data, target = data.cuda(), target.cuda()

        # Forward pass.
        optimizer.zero_grad() # very important 
        data, target = Variable(data), Variable(target)
        output = net(data)
        results.append((output.data.cpu(), target.data.cpu()))
        # Backward pass and optimization step.
        loss = criterion(output, target)
        loss_logger.update(loss.data[0])
        loss.backward()
        optimizer.step()

        # Compute time elapsed on batch.
        batch_time.update(time.time() - end)
        end = time.time()

        # Compute logging information.
        nProcessed += len(data)
        print('Train Epoch: {} [{}/{} ({:.0f}%)]\tLoss: {loss.val:.6f} ({loss.avg:.6f})'
              '\tTime {batch_time.val:.3f} -- '
              'Data {data_time.val:.3f}\t'.format(
            epoch, nProcessed, nTrain, 100. * batch_idx / len(train_loader),
            loss = loss_logger, batch_time = batch_time, data_time = data_time))
  
        trainF.write('{},{}\n'.format(epoch, loss.data[0]))
        trainF.flush()
    predictions = torch.cat([entry[0] for entry in results], 0)
    targets = torch.cat([entry[1] for entry in results], 0)
    _, _, _, f1_unknown_gender, _, _ = accuracy(predictions, targets)
    f1score_log.write('{},{},{}\n'.format(epoch, 'train', f1_unknown_gender))
    f1score_log.flush()
    # _, pred_train = inference.inference(predictions.tolist())
    # pickle.dump(pred_train, open(os.path.join(args.log_dir, 'pred_train'), "wb"))


def test(args, epoch, net, criterion, test_loader, optimizer, testF, test_log, f1score_log):
    net.eval()
    test_loss = 0
    results = list()
    for batch_idx, (data, target, image_ids) in enumerate(test_loader):
        # Prepare inputs.
        data, target = data.cuda(), target.cuda()
        data, target = Variable(data, volatile = True), Variable(target)

        output = net(data)
        results.append((output.data.cpu(), target.data.cpu()))
        # accuracy(output, target)
        batch_loss = criterion(output, target).data[0]

        # Logging information.
        if batch_idx % 50 == 0:
            inference.write_file(output.data.cpu().tolist(), target.data.cpu().tolist(), image_ids, test_log, "val", args)
        test_loss += batch_loss
        print('Test Epoch: {} [{}/{}]\tLoss: {:.6f}\t'.format(
            epoch, batch_idx, len(test_loader), batch_loss))

    test_loss /= len(test_loader) 
    nTotal = len(test_loader.dataset)
    print('\nTest set: Average loss: {:.4f}\n'.format(test_loss))

    testF.write('{},{}\n'.format(epoch, test_loss))
    testF.flush()
    predictions = torch.cat([entry[0] for entry in results], 0)
    targets = torch.cat([entry[1] for entry in results], 0)
    _, _, _, f1_unknown_gender, _, _ = accuracy(predictions, targets)
    f1score_log.write('{},{},{}\n'.format(epoch, 'test', f1_unknown_gender))
    f1score_log.flush()
    # _, pred_dev = inference.inference(predictions.tolist())
    # pickle.dump(pred_dev, open(os.path.join(args.log_dir, 'pred_dev'), "wb"))
    return 1 - f1_unknown_gender

from sklearn.metrics import f1_score
from sklearn.metrics import precision_score
from sklearn.metrics import recall_score

def accuracy(output, target):
    output_gender = output[:, :2]
    target_gender = target[:, :2]
    output_objects = output[:, 2:].contiguous().view(output.size(0), 2,80,2)
    target_objects = target[:, 2:].contiguous().view(output.size(0), 2,80,2)

    #given man/woman
    pred_scores = torch.Tensor(output_objects.size(0),1, 80, 2)
    for i in range(output.size(0)):
        # pdb.set_trace()
        if target_gender[i, 1] == 0:
            pred_scores[i, 0, :, :] = output_objects[i, 0, :, :]
        else:
            pred_scores[i, 0, :, :] = output_objects[i, 1, :, :]

    # pred_scores = torch.index_select(output_objects, 1, target_gender[:, 1]) #batch_size*1*80*2
    _, pred_idx = torch.min(pred_scores, 3)
    pred_idx = pred_idx.squeeze() #batch_size*80

    target_scores = torch.Tensor(target_objects.size(0),1, 80, 2)
    for i in range(output.size(0)):
        if target_gender[i, 1] == 0:
            target_scores[i, 0, :, :] = target_objects[i, 0, :, :]
        else:
            target_scores[i, 0, :, :] = target_objects[i, 1, :, :]

    # target_scores = torch.index_select(target_objects, 1, target_gender[:, 1]) #batch_size*1*80*2
    _, target_idx = torch.min(target_scores, 3)
    target_idx = target_idx.squeeze()

    pred_scores_unknown = torch.Tensor(output_objects.size(0),1, 80, 2)
    for i in range(output.size(0)):
        if output_gender[i, 1] == 0:
            pred_scores_unknown[i, 0, :, :] = output_objects[i, 0, :, :]
        else:
            pred_scores_unknown[i, 0, :, :] = output_objects[i, 1, :, :]
    # pred_scores_unknown = torch.index_select(output_objects, 1, output_gender[:, 1]) #batch_size*1*80*2
    _, pred_idx_unknown = torch.min(pred_scores_unknown, 3)
    pred_idx_unknown = pred_idx_unknown.squeeze() #batch_size*80

    target_idx = target_idx.numpy()
    pred_idx = pred_idx.numpy()
    pred_idx_unknown = pred_idx_unknown.numpy()

    # print("f1 score for gender:...")
    # _, output_gender = torch.max(output_gender, 1)
    # output_gender = output_gender.squeeze()
    # _, target_gender = torch.max(target_gender,1)
    # target_gender = target_gender.squeeze()
    # print(f1_score(target_gender.numpy(), output_gender.numpy(), average='macro'))    

    f1_known_gender = f1_score(target_idx, pred_idx, average='macro')
    # f1_class = list()
    # for i in range(80):
    #     f1_class.append(f1_score(target_idx[:,i], pred_idx[:, i]))
    # print(f1_class)
    # print(sum(f1_class) / float(len(f1_class)))

    recall_known_gender = recall_score(target_idx, pred_idx, average='macro')  
    precision_known_gender = precision_score(target_idx, pred_idx, average='macro')

    f1_unknown_gender = f1_score(target_idx, pred_idx_unknown, average='macro')
    recall_unknown_gender = recall_score(target_idx, pred_idx_unknown, average='macro')  
    precision_unknown_gender = precision_score(target_idx, pred_idx_unknown, average='macro')

    print f1_known_gender, precision_known_gender, recall_known_gender, f1_unknown_gender, precision_unknown_gender, recall_unknown_gender

    return f1_known_gender, precision_known_gender, recall_known_gender, f1_unknown_gender, precision_unknown_gender, recall_unknown_gender



class AverageMeter(object):
    """Computes and stores the average and current value"""
    def __init__(self):
        self.reset()

    def reset(self):
        self.val = 0
        self.avg = 0
        self.sum = 0
        self.count = 0

    def update(self, val, n=1):
        self.val = val
        self.sum += val * n
        self.count += n
        self.avg = self.sum / self.count

if __name__ == '__main__':
    main()