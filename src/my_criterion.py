import torch.nn as nn
import itertools
import numpy as np
import torch
from torch.autograd import Variable
import pdb
import torch.autograd as autograd
import torch.optim as optim
import pdb

torch.manual_seed(1)

class CRF_Loss(nn.Module):

    def __init__(self, num_verb, num_role):
        super(CRF_Loss, self).__init__()
        self.num_verb = num_verb
        self.num_role = num_role

    def forward(self, output, target):
        top = torch.sum(torch.mul(output, target), 1)
        output_gender = output[:, :2]
        target_gender = target[:, :2]
        output_objects = output[:, 2:].contiguous().view(output.size(0), 2,80,2)
        target_objects = target[:, 2:].contiguous().view(output.size(0), 2,80,2)
        output_objects = self.log_sum_exp(output_objects, 3).squeeze() # batch_size * ver_num * object_num
        output_objects = torch.sum(output_objects, 2).squeeze()
        output_score = torch.add(output_gender, output_objects)
        output_score = self.log_sum_exp(output_score, 1).squeeze()
        return torch.sum(output_score - top)

    def log_sum_exp(self, m, dim):
        max_score, idx = torch.max(m, dim)
        max_score_broadcast = max_score.expand(m.size())
        return max_score + torch.log(torch.sum(torch.exp(m - max_score_broadcast), dim))



