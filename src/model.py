import pdb
import torch
import torch.nn as nn
import torch.nn.functional as F
import torchvision.models as models
from torch.autograd import Variable
import torch.nn.utils
from torch.nn.utils.rnn import pack_padded_sequence
from torch.nn.utils.rnn import pad_packed_sequence

class CocoObjectCRF(nn.Module):

    def __init__(self, num_verb, num_role, num_value):

        super(CocoObjectCRF, self).__init__()

        self.num_verb = num_verb
        self.num_role = num_role
        self.num_value = num_value

        self.base_network = models.resnet50(pretrained = True)
        output_size = num_verb + num_role*num_verb*num_value
        self.finalLayer = nn.Linear(2048, output_size)

    def forward(self, image):
        x = self.base_network.conv1(image)
        x = self.base_network.bn1(x)
        x = self.base_network.relu(x)
        x = self.base_network.maxpool(x)

        x = self.base_network.layer1(x)
        x = self.base_network.layer2(x)
        x = self.base_network.layer3(x)
        x = self.base_network.layer4(x)

        image_features = x.max(dim = 2)[0].max(dim = 3)[0].squeeze()
        preds = self.finalLayer(image_features)

        return preds