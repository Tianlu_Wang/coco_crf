#!/usr/bin/env python3

import argparse
import os
import numpy as np
import pdb

import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
plt.style.use('bmh')

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('expDir', type = str)
    args = parser.parse_args()
    trainP = os.path.join(args.expDir, 'train.csv')
    trainData = np.loadtxt(trainP, delimiter=',').reshape(-1, 2)
    testP = os.path.join(args.expDir, 'test.csv')
    testData = np.loadtxt(testP, delimiter=',').reshape(-1, 2)
    f1scoreP = os.path.join(args.expDir, 'f1score.csv')
    f1scoreData = np.loadtxt(f1scoreP, delimiter=',', 
                              dtype={'names': ('epoch', 'split', 'f1score'), 
                                     'formats': ('i4', 'S5', 'f4')})
    trainI, trainLoss = np.split(trainData, 2, axis=1)
    trainI, trainLoss = [x.ravel() for x in (trainI, trainLoss)]
    
    trainI_ = np.unique(trainI)
    trainLoss_ = np.zeros_like(trainI_)
    for (idx, epoch) in enumerate(trainI_):
        ids = np.nonzero(trainI == epoch)[0]
        trainLoss_[idx] = trainLoss[ids].mean()

    testI, testLoss = np.split(testData, 2, axis = 1)
    epochI = np.array([x[0] for x in f1scoreData])
    split = np.array([x[1] for x in f1scoreData])
    f1score = np.array([x[2] for x in f1scoreData])

    fig, ax = plt.subplots(1, 1, figsize = (12, 10))
    # plt.plot(trainI, trainLoss, label='Train')
    plt.plot(trainI_, trainLoss_, label = 'Train')
    plt.plot(testI, testLoss, label = 'Test')
    plt.xlabel('Epoch')
    plt.xticks(np.arange(min(trainI_), max(trainI_) + 1, 1.0))
    plt.ylabel('Loss')
    plt.legend()
    loss_fname = os.path.join(args.expDir, 'loss.png')
    plt.savefig(loss_fname)
    print('Created {}'.format(loss_fname))

    # fig, ax = plt.subplots(1, 1, figsize = (6, 5))
    # # plt.plot(trainI, trainErr, label='Train')
    # plt.plot(trainI_, trainErr_, label = 'Train')
    # plt.plot(testI, testErr, label = 'Test')
    # plt.xlabel('Epoch')
    # plt.xticks(np.arange(min(trainI_), max(trainI_) + 1, 1.0))
    # plt.ylabel('Error')
    # plt.legend()
    # err_fname = os.path.join(args.expDir, 'error.png')
    # plt.savefig(err_fname)
    # print('Created {}'.format(err_fname))
    train_ids = np.nonzero([x == 'train' for x in split])[0].astype(int)
    test_ids = np.nonzero([x == 'test' for x in split])[0].astype(int)
    trainEpochI = epochI[train_ids]
    testEpochI = epochI[test_ids]
    trainf1score = f1score[train_ids]
    testf1score = f1score[test_ids]

    fig, ax = plt.subplots(1, 1, figsize = (12, 10))
    # plt.plot(trainI, trainErr, label='Train')
    plt.plot(trainEpochI, trainf1score, label = 'Train')
    plt.plot(testEpochI, testf1score, label = 'Test')
    plt.xlabel('Epoch')
    plt.xticks(np.arange(min(trainEpochI), max(trainEpochI) + 1, 1.0))
    plt.ylabel('f1score')
    plt.legend()
    f1score_fname = os.path.join(args.expDir, 'f1score.png')
    plt.savefig(f1score_fname)
    print('Created {}'.format(f1score_fname))


    # loss_err_fname = os.path.join(args.expDir, 'loss-error-f1score.png')
    # os.system('convert +append {} {} {} {}'.format(loss_fname, err_fname, f1score_fname, loss_err_fname))
    # print('Created {}'.format(loss_err_fname))

if __name__ == '__main__':
    main()